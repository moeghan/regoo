<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class Intro extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $product_count = \Cache::remember('product_count', 5, function() {
            $product_count = \DB::table('products')->orderBy('id', 'desc')->limit(1)->pluck('id');
            $product_count = collect($product_count)->first();

            return $product_count;
        });


        return view("widgets.intro", [
            'config' => $this->config,
            'product_count' => $product_count
        ]);
    }
}