<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Marketplace;

class ProductFilter extends AbstractWidget
{
    public $cacheTime = 44640;
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'current_input' => null,
        'current_path' => null,
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {   
        $locations = \App\Product::groupBy('location')->orderBy('location', 'asc')->pluck('location');

        return view("widgets.product_filter", [
            'config' => $this->config,
            'marketplaces' => Marketplace::all(),
            'locations' => $locations,

        ]);
    }
}