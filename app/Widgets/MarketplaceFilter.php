<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use DB;
use App\Marketplace;

class MarketplaceFilter extends AbstractWidget
{
    public $cacheTime = 44640;
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
    
        $ids = $this->config['ids'];
        $total = $this->config['total'];
        $ratio = round($total/20);

        $marketplaces = DB::table('products')
        ->select(DB::raw('count(products.id) as total, marketplaces.slug, marketplaces.name'))
        ->whereIn('products.id', $ids)
        ->join('marketplaces', 'marketplaces.id', '=', 'marketplace_id')
        ->groupBy('marketplaces.slug')
        ->get();

        return view("widgets.marketplace_filter", [
            'config' => $this->config,
            'marketplaces' => $marketplaces,
            'ratio' => $ratio
        ]);
    }
}