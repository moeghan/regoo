<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class LikeButton extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $product = $this->config['product'];

        return view("widgets.like_button", [
            'config' => $this->config,
            'product' => $product
        ]);
    }
}