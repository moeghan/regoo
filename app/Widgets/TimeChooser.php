<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Carbon\Carbon;
use DB;

class TimeChooser extends AbstractWidget
{
    public $cacheTime = 44640;
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        
        $ids = $this->config['ids'];
        $total = $this->config['total'];
        $ratio = round($total/20);


        $times = \App\Product::whereIn('id', $ids)->select(
            DB::raw('count(*) as total'),
            DB::raw("
            CASE
                WHEN created_at > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 7 DAY)) THEN 'Seminggu Terakhir'
                WHEN created_at > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 30 DAY)) THEN 'Sebulan Terakhir'
                WHEN created_at > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 90 DAY)) THEN '3 Bulan Terakhir'
                WHEN created_at > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 365 DAY)) THEN 'Setahun Terakhir'
            END
            AS date_interval"),
            DB::raw("
            CASE
                WHEN created_at > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 7 DAY)) 
                    THEN DATE_FORMAT(SUBDATE(now(), INTERVAL 7 DAY), '%Y-%m-%d')
                WHEN created_at > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 30 DAY)) 
                    THEN DATE_FORMAT(SUBDATE(now(), INTERVAL 30 DAY), '%Y-%m-%d')
                WHEN created_at > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 90 DAY)) 
                    THEN DATE_FORMAT(SUBDATE(now(), INTERVAL 90 DAY), '%Y-%m-%d')
                WHEN created_at > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 365 DAY)) 
                    THEN DATE_FORMAT(SUBDATE(now(), INTERVAL 365 DAY), '%Y-%m-%d')
            END
            AS date_filter")
        )
        ->get('total', 'date_interval', 'date_filter');

        return view("widgets.time_chooser", [
            'config' => $this->config,
            'times' => $times,
            'ratio' => $ratio

        ]);
    }
}