<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use DB;
use App\Product;

class PriceChooser extends AbstractWidget
{
    public $cacheTime = 44640;
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $ids = $this->config['ids'];
        $total = $this->config['total'];
        $ratio = round($total/20);

        $prices = Product::whereIn('id', $ids)->select(DB::raw('count(*) as total'), DB::raw("
            CASE
                WHEN price BETWEEN 0 AND 100000 THEN '0-100rb'
                WHEN price BETWEEN 100000 AND 500000 THEN '100rb-500rb'
                WHEN price BETWEEN 500000 AND 1000000 THEN '500rb-1jt'
                WHEN price BETWEEN 1000000 AND 2500000 THEN '1jt-2,5jt'
                WHEN price BETWEEN 2500000 AND 5000000 THEN '2,5jt-5jt'
                WHEN price BETWEEN 5000000 AND 25000000 THEN '5jt-25jt'
                WHEN price BETWEEN 25000000 AND 50000000 THEN '25jt-50jt'
                WHEN price BETWEEN 50000000 AND 100000000 THEN '50jt-100jt'
            END
            AS round_price"),
            DB::raw("
            CASE
                WHEN price BETWEEN 0 AND 100000 THEN '0'
                WHEN price BETWEEN 100000 AND 500000 THEN '100000'
                WHEN price BETWEEN 500000 AND 1000000 THEN '500000'
                WHEN price BETWEEN 1000000 AND 2500000 THEN '1000000'
                WHEN price BETWEEN 2500000 AND 5000000 THEN '2500000'
                WHEN price BETWEEN 5000000 AND 25000000 THEN '5000000'
                WHEN price BETWEEN 25000000 AND 50000000 THEN '25000000'
                WHEN price BETWEEN 50000000 AND 100000000 THEN '50000000'
            END
            AS min"),
            DB::raw("
            CASE
                WHEN price BETWEEN 0 AND 100000 THEN '100000'
                WHEN price BETWEEN 100000 AND 500000 THEN '500000'
                WHEN price BETWEEN 500000 AND 1000000 THEN '1000000'
                WHEN price BETWEEN 1000000 AND 2500000 THEN '2500000'
                WHEN price BETWEEN 2500000 AND 5000000 THEN '5000000'
                WHEN price BETWEEN 5000000 AND 25000000 THEN '25000000'
                WHEN price BETWEEN 25000000 AND 50000000 THEN '50000000'
                WHEN price BETWEEN 50000000 AND 100000000 THEN '100000000'
            END
            AS max")
        )
        ->groupBy('round_price')
        ->get('round_price','total', 'min', 'max');

        return view("widgets.price_chooser", [
            'config' => $this->config,
            'prices' => $prices,
            'ratio' => $ratio
        ]);
    }
}