<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sitemap extends Model
{
    protected $fillable = ['lastmod', 'loc', 'marketplace_id'];

    public function marketplace()
    {
        return $this->belongsTo('App\Marketplace');
    }
}
