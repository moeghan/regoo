<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Marketplace;
use App\Product;
use Sitemap;
use Carbon\Carbon;


class SitemapController extends Controller
{
	public function index()
	{
		Sitemap::addSitemap(url('sitemap-general.xml'));
		
		$product_count = Product::count();
		$pages = (int)(round($product_count/config('site.sitemap.products_per_sitemap'), 0, PHP_ROUND_HALF_DOWN))+1;

		for ($page = 1; $page <= $pages; $page++) {
			Sitemap::addSitemap(route('sitemap.product', ['page' => $page]));
		}


		return Sitemap::index();
	}

	public function general()
	{
		Sitemap::addTag(url(''), Carbon::now(), 'daily', '0.8');
		$marketplaces = Marketplace::with('categories')->get();

		foreach ($marketplaces as $marketplace) {
			
			$marketplace_url = route('marketplace.show', ['slug' => $marketplace->slug]);
			Sitemap::addTag($marketplace_url, Carbon::now(), 'daily', '0.8');

			$categories = $marketplace->categories()->where('categories.level', '=', 1)->get();

			foreach ($categories as $category) {
				Sitemap::addTag(build_category_url($category->slug, $marketplace_url), Carbon::now(), 'daily', '0.8');
			}

			// add terms to sitemap
			foreach (save_or_get_term() as $term) {	
				Sitemap::addTag(url($term['url']), Carbon::now(), 'weekly', '0.8');
			}

		}

		return Sitemap::render();
	}

	public function product()
	{
		$products = Product::with('categories', 'marketplace')->paginate(config('site.sitemap.products_per_sitemap'));

		foreach ($products as $product) {
			$url = build_product_url_for($product);
			Sitemap::addTag($url, $product->updated_at, 'monthly', '0.4');
		}

		return Sitemap::render();

	}
}
