<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Marketplace;
use App\Category;
use App\Product;
use Route;
use SEOMeta;

class MarketplaceController extends Controller
{
    public function show($slug)
    {
    	$marketplace = Marketplace::remember(86400)->where('slug', '=', $slug)->firstOrFail();

        $products = Product::remember(60)->with(Product::cacheable())->where('marketplace_id', '=', $marketplace->id)->orderBy('id', 'desc')->simplePaginate(20);
        $category_parent = null;

        $title = $marketplace->name;
        SEOMeta::setTitle($title);
    	
    	return view('marketplaces.show', [
    		'marketplace' => $marketplace,
            'products' => $products,
            'category' => null,
            'title' => $title,
    		]);
    }
}
