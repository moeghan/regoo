<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Marketplace;
use App\Category;
use App\Product;
use App;
use SEOMeta;

class CategoryController extends Controller
{
    public function show($marketplace_slug, $hierarchy)
    {
    	$marketplace = Marketplace::remember(5)->where('slug', '=', $marketplace_slug)->first();
    	$hierarchy = explode('/', $hierarchy);

        $slug = end($hierarchy);

        $exploded_slug = explode('-', $slug);
        $id = $exploded_slug[0];
        if(is_numeric($id)){
            // cache for one week
            $product = Product::remember(10080)->with(Product::cacheable())->find($id);

            if(!is_null($product)){

                array_pop($hierarchy);
                $slug = end($hierarchy);


                $category = $marketplace->categories()->remember(10080)->where('slug', '=', $slug)->first();

                $title = $marketplace->name . ': ' . $product->name;
                SEOMeta::setTitle($title);
                $related_products = $product->related();

                return view('products.show', [
                    'product' => $product,
                    'marketplace' => $marketplace,
                    'category' => $category,
                    'title' => $title,
                    'related_products' => $related_products
                    ]);
            }
        }


	    $category = $marketplace->categories()->remember(60)->with([
            'marketplace' => function($q){
                $q->remember(5);
            }
            ])->where('slug', '=', $slug)->first();

	    $products = $category->products()->with(Product::cacheable())->remember(60)->orderBy('id', 'desc')->simplePaginate(20);

        $title = 'Produk Kategori '. ucwords($category->name) .' di ' . $marketplace->name;

        SEOMeta::setTitle($title);

    	return view('marketplaces.show', [
    		'marketplace' => $marketplace,
    		'category' => $category,
    		'products' => $products,
            'title' => $title,
    		]);

	    App::abort('404');
    }
}
