<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Marketplace;
use App\Product;
use SEOMeta;
use DB;
use Auth;

class ProductController extends Controller
{
    public function like(Request $request, $id)
    {
        if ($request->ajax() && Auth::user()) {

            $product = Product::where('id', '=', $id)->firstOrFail();

            $product->like();

            return $product->likeCount;
        }
    }

    public function unlike(Request $request, $id)
    {
        if ($request->ajax() && Auth::user()) {

            $product = Product::where('id', '=', $id)->firstOrFail();

            $product->unlike();

            return $product->likeCount;
        }
    }

    public function index(Request $request)
    {
        $filters = [];
        $sort = [];
        $q = request()->input('query');

        $filters = $request->input();
        if(isset($filters['marketplace:slug']) && $filters['marketplace:slug'] == '%%'){
            unset($filters['marketplace:slug']);
        }


        if(isset($filters['sort'])){
            $sort = $filters['sort'];
        }

        if(isset($filters['sort']) && $filters['sort'] == 'relevance'){
            unset($filters['sort']);
            $sort = [];
        }

        if(isset($filters['price'])){
            if((int)$filters['price']['min'] > 0){

                $filters['price'][] = '(ge)' . (int)$filters['price']['min'];
            }

            unset($filters['price']['min']);


            if((int)$filters['price']['max'] > 0){
                
                $filters['price'][] = '(le)' . (int)$filters['price']['max'];
            }

            unset($filters['price']['max']);
        }

        if(empty($filters['price'])){
            unset($filters['price']);
        }

        $title = build_title_from_array($filters);

        $products = Product::search($q, $filters, $sort);

    	$marketplace = null;

    	if($request->input('marketplace_id', false)){
    		$marketplace = Marketplace::find($request->input('marketplace_id'));
     	}
        
        $total = Product::total($products);
        $products = $products->simplePaginate(20);
        $ids = $products->pluck('id');

        if(!$products->isEmpty() && !empty($request->input('query'))){
            // save current term & url to redis
            $url = str_replace(url(''), '', $request->fullUrl());
            $term = str_replace('harga', '', $title);
            
            save_or_get_term($term, $url);
        }

        $title = $title;
        SEOMeta::setTitle(ucwords($title));

    	return view('products.index', [
    		'products' => $products,
    		'title' => $title,
    		'query' => $filters,
    		'marketplace' => $marketplace,
            'q' => $q,
            'total' => $total,
            'ids' => $ids,
    		]);
    }

    public function redirect(Request $request)
    {
        if($request->input('u')){
            return redirect()->to(base64url_decode($request->input('u')));
        }
    }
}
