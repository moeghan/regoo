<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Marketplace;
use App\Jobs\BukalapakSitemapUrlToCategories;
use Redis;


class BukalapakScraper extends Command
{
    protected $sitemap_url;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:bukalapak';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape bukalapak.com products and returns the products in JSON format, so the products can be easily imported';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $app = require __DIR__.'/../../../bootstrap/app.php';

        $app->make(\Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        $this->sitemap_url = config('site.bukalapak.sitemap_url');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(Redis::llen('queues:bukalapak:sitemap_url_to_categories')){
            $this->info('Bukalapak scraper already running, please wait until it dispatched');
            return;
        }

        if(env('APP_ENV') !== 'testing')
            $this->info('Running bukalapak scraper');

        $marketplace = Marketplace::firstOrCreate(config('site.bukalapak.seed'));
        dispatch((new BukalapakSitemapUrlToCategories($this->sitemap_url))->onQueue('bukalapak:sitemap_url_to_categories'));
    }
}
