<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Jedrzej\Pimpable\PimpableTrait;
use Watson\Rememberable\Rememberable;
use Phumbor;
use Cache;
use Conner\Likeable\LikeableTrait;

class Product extends Model
{
    use PimpableTrait;
    use Rememberable;
    use LikeableTrait;

    protected $searchableColumns = [
        'name' => 100, 
        'description' => 10,
        'categories.name' => 5,
        'marketplace.name' => 5
    ];

    public $searchable = ['*'];
    public $sortable = ['*'];

    public $notSearchable = ['view_as', 'query', 'sort', 'page', 'debug'];
    
    protected $fillable = ['loc', 'lastmod'];

    public static function cacheable()
    {
        return config('site.product.relations.cacheable');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function getImageAttribute($value)
    {
        $value = str_replace('https://', '', $value);
        $value = str_replace('http://', '', $value);

        return Phumbor::url($value);
    }

    public function getUrlAttribute()
    {
        return base64url_encode($this->loc);
    }

    public function marketplace()
    {
        return $this->belongsTo('App\Marketplace');
    }

    public function related($limit = 10)
    {
        // search match case first
        $products = $this::search($this->name)->simplePaginate($limit);

        return $products;
    }

    public static function search($q, $filters = [], $sort = [])
    {
        // cache for one month
        $products =  self::remember(44640)->with(self::cacheable());
        
        if(!empty($q)){

            
            $products = $products->whereRaw('MATCH (name,description) AGAINST(?)', [$q]);
        }

        if(!empty($filters)){
            if(is_null($products)){
                $products = self::filtered($filters);
            }
            else{
                $products = $products->filtered($filters);
            }
        }
        if(!empty($sort)){
            if(is_null($products)){
                $products = self::sorted($sort);
            }
            else{
                $query = $products;
                $subquery = $query->getQuery();
                $subquery->orders = [];
                $query->setQuery($subquery);

                $products = $products->sorted($sort);
            }
        }

        return $products;
    }

    public static function total($products)
    {
        $query = $products->toSql() . '|||' . implode('||', $products->getBindings());
        $cacheKey = md5($query);

        $product_count = Cache::remember($cacheKey, 10080, function() use ($products){
            $product_count = 0;
            
            if($products->select('*')->skip(100)->take(1)->first()){
                return 100;
            }

            if($products->select('*')->skip(10)->take(1)->first()){
                return 10;
            }

            if($products->select('*')->first()){
                return 1;
            }

            return 0;
        });

        return $product_count;    
    }

    public static function getLikes($user_id) {
        return (new static)
            ->join('likeable_likes', 'products.id', '=', 'likeable_likes.likable_id')
            ->select('products.*')
            ->where('likeable_likes.user_id', '=', $user_id)
            ->orderBy('likeable_likes.created_at', 'DESC');
    }

}
