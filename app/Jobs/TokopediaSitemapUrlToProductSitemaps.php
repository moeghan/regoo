<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Cache;
use Carbon\Carbon;
use Symfony\Component\DomCrawler\Crawler;


class TokopediaSitemapUrlToProductSitemaps extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $sitemap_index = '';
    protected $sitemaps = [];
    protected $sitemap_url = '';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sitemap_url)
    {
        $this->sitemap_url = $sitemap_url;
        $app = require __DIR__.'/../../bootstrap/app.php';

        $app->make(\Illuminate\Contracts\Console\Kernel::class)->bootstrap();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->enqueueSitemaps();
    }

    public function getSitemapIndex()
    {
        $this->sitemap_index = Cache::remember('sitemap_index', config('site.cache_duration'), function(){

            return @file_get_contents($this->sitemap_url);
        });
    }

    public function getSitemaps()
    {
        if(empty($this->sitemap_index)){
            $this->getSitemapIndex();
        }

        $this->sitemaps = Cache::remember('sitemaps', config('site.cache_duration'), function(){
            $crawler = new Crawler($this->sitemap_index);
            $sitemaps = [];

            $nodes = $crawler->filter('sitemap');

            foreach ($nodes as $key => $node) {
                if($key == 4 && env('APP_ENV') === 'testing'){
                    break;
                }

                $node = new Crawler($node);

                $sitemap['loc'] = $node->filter('loc')->text();
                $sitemap['lastmod'] = new Carbon($node->filter('lastmod')->text());

                $sitemaps[] = $sitemap;

                // free up memory
                free_memory($sitemap);
                free_memory($node);
                free_memory($i);
            }

            // free up memory
            free_memory($crawler);
            krsort($sitemaps);
            
            return $sitemaps;
        });

        return $this->sitemaps;
    }

    public function enqueueSitemaps()
    {
        if(empty($this->sitemaps)){
            $this->getSitemaps();
        }

        foreach ($this->sitemaps as $sitemap) {
            $this->enqueueSitemap($sitemap);
        }
    }

    public function enqueueSitemap($sitemap)
    {
        dispatch((new TokopediaProductSitemapToProductUrls($sitemap))->onQueue('tokopedia:product_sitemap_to_product_urls'));
    }
}
