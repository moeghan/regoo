<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Symfony\Component\DomCrawler\Crawler;


class BukalapakCategoryPageToProductUrls extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $category_page_url;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($category_page_url)
    {
        $this->category_page_url = $category_page_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->enqueueProductUrls();
    }

    public function enqueueProductUrls()
    {
        $crawler = new Crawler(file_get_contents($this->category_page_url));

        $nodes = $crawler->filter('div.basic-products ul li.product');

        foreach ($nodes as $key => $node) {
            if($key == 4 && env('APP_ENV') == 'testing'){
                break;
            }

            $crawler = new Crawler($node);
            $product_url = 'https://www.bukalapak.com' . $crawler->filter('div.product-media a')->attr('href');

            $this->enqueueProductUrl($product_url);
        }

        free_memory($crawler);
        free_memory($nodes);
        free_memory($key);
        free_memory($node);
        free_memory($product_url);
    }

    public function enqueueProductUrl($product_url)
    {
        $job = (new BukalapakProductUrlToProduct($product_url))
        ->onQueue('bukalapak:product_url_to_product');
        dispatch($job);

        free_memory($job);
    }
}
