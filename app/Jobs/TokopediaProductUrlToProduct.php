<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Marketplace;
use App\Category;
use App\Product;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Symfony\Component\DomCrawler\Crawler;
use DB;
use Log;


class TokopediaProductUrlToProduct extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $config, $product;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($product)
    {
        $this->product = $product;

        if(is_null(app())){
            $app = require __DIR__.'/../../bootstrap/app.php';

            $app->make(\Illuminate\Contracts\Console\Kernel::class)->bootstrap();
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {

            $logging = env('APP_DEBUG', false);
            if($logging) Log::info(":::::::::");

            $toped = Marketplace::firstOrCreate(config('site.tokopedia.seed'));

            $url = trim($this->product['loc']);
            if(empty($url)){
                if($logging) Log::info('URL is empty, skipping');
                return;
            }

            $url = ['loc' => $this->product['loc']];
            $product = Product::firstOrNew($url);

            if($logging) {
                Log::info('Product exists? ' . (string)$product->exists);
            };

            if($product->exists && config('site.crawler.skip_if_exists')){
                Log::info('Skip crawling because of configuration: config/site.php|crawler.skip_if_exists');
                return;
            }

            $product->marketplace_id = $toped->id;

            $options  = array('http' => array('user_agent' => 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'));
            $context  = stream_context_create($options);

            if($logging) Log::info('Getting content from: ' . $this->product['loc']);

            $content = @file_get_contents($this->product['loc'], false, $context);

            if(!$content){
                if($logging) Log::info('Failed');
                return;
            }

            $crawler = new Crawler($content);
            $product->name = $crawler->filter('h1.product-title a')->text();
            $product->slug = str_slug($product->name);
            $product->image = $crawler->filter('div.product-imagebig img')->attr('src');
            $product->location = $crawler->filter('#b-p-info-penjual > div.product-box-content > div.rowfluid > div.span12.mt-10.ml-0.text-center > div:nth-child(4) > span > span > span')->text();
            $product->description = strip_tags($crawler->filter('div.product-content-container p')->html(), '<br>');
            $product->price = str_replace('.', '', $crawler->filter('span[itemprop="price"]')->text());
            $product->price_reduced = $product->price;
            $product->condition = trim(strtolower($crawler->filter('dl > dd:nth-child(10)')->text()));
            $product->save();

            if($logging) Log::info('Save product with id: ' . $product->id);

            $categories = [];

            $crawler->filter('ul[itemprop="breadcrumb"] li a')->each(function($node, $i) use (&$categories){
                if(trim($node->text()) !== 'Beranda'){
                    $categories[] = trim($node->text());
                }
            });


            foreach ($categories as $key => $category) {
                $cat = $toped->categories()->firstOrCreate([
                    'name' => $category,
                    'slug' => str_slug($category),
                    'level' => $key+1,
                ]);


                if($key > 0){
                    $cat->parent_id = $toped->categories()->where('name', '=', $categories[$key-1])->first()->id;
                    $cat->save();
                }

                $product->categories()->sync([$cat->id], false);

                // free memory
                free_memory($cat);
                free_memory($category);
                free_memory($key);
            }

            if($logging) Log::info('Save product categories with id: ' . $product->categories->pluck('id')->implode(', '));


            // free memory
            free_memory($toped);
            free_memory($url);
            free_memory($options);
            free_memory($context);
            free_memory($logging);
            free_memory($product_exists);
            free_memory($product);
            free_memory($content);
            free_memory($categories);
            free_memory($crawler);
        });



    }
}
