<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Marketplace;
use App\Category;
use App\Product;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Symfony\Component\DomCrawler\Crawler;
use Carbon\Carbon;
use DB;

class BukalapakProductUrlToProduct extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $product_url;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($product_url)
    {
        $this->product_url = $product_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->extractProduct($this->product_url);
    }

    public function extractProduct($product_url)
    {
        DB::transaction(function () {

            $product = Product::firstOrCreate(['loc' => $this->product_url]);
            $crawler = new Crawler(file_get_contents($this->product_url));
            $marketplace = Marketplace::firstOrCreate(config('site.bukalapak.seed'));

            $product->lastmod = Carbon::now();
            $product->marketplace_id = $marketplace->id;
            $product->name = trim($crawler->filter('h1[itemprop="name"]')->text());
            $product->slug = str_slug($product->name);
            $product->image = $crawler->filter('img[itemprop="image"]')->attr('src');
            $product->location = trim($crawler->filter('div.user-address')->text());
            $product->description = trim(strip_tags($crawler->filter('div.js-collapsible')->html(), '<br><p>'));
            $product->condition = trim(strtolower($crawler->filter('span.product__condition')->text()));

            $price = $crawler->filter('span[itemprop="price"]');
            if(count($price) == 1){

                $product->price = str_replace('.', '', $crawler->filter('span[itemprop="price"]')->text());
                $product->price_reduced = $product->price;
            }

            if(count($price) == 2){
                $product->price = str_replace('.', '', $price->eq(0)->text());
                $product->price_reduced = str_replace('.', '', $price->eq(1)->text());
            }

            $product->save();


            $categories = [];

            $crawler->filter('nav#breadcrumb ol li a')->each(function($node, $i) use (&$categories){
                if(trim($node->text()) !== 'Home'){
                    $categories[] = trim($node->text());
                }
            });


            foreach ($categories as $key => $category) {
                $cat = $marketplace->categories()->firstOrCreate([
                    'name' => $category,
                    'slug' => str_slug($category),
                    'level' => $key+1,
                ]);


                if($key > 0){
                    $cat->parent_id = $marketplace->categories()->where('name', '=', $categories[$key-1])->first()->id;
                    $cat->save();
                }

                $product->categories()->sync([$cat->id], false);

                // free memory
                free_memory($cat);
                free_memory($category);
                free_memory($key);
            }

            // free up memory
            free_memory($product);
            free_memory($crawler);
            free_memory($marketplace);
            free_memory($price);
            free_memory($category_nodes);
            free_memory($key);
            free_memory($node);
            free_memory($category);
            free_memory($cat);
        });

    }
}
