<?php

namespace App;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

class Category extends Model
{
    use Rememberable;
    
    protected $fillable = ['name', 'slug', 'marketplace_id', 'level'];
    use NodeTrait;

    public function marketplace()
    {
        return $this->belongsTo('App\Marketplace');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
