<?php
return [
    // filter produk berdasarkan:
    'kondisi' => [
        'icon' => 'clock-o',
        'items' => [
            'baru' => [
                [
                    'column' => 'condition',
                    'operator' => '=',
                    'value' => 'used',
                ]
            ],
            'bekas' => [
                [
                    'column' => 'condition',
                    'operator' => '=',
                    'value' => 'used',
                ]
            ],
        ]
    ],
    'harga' => [
        'icon' => 'dollar',
        'items' => [
            '100rb' => [
                [
                    'column' => 'price',
                    'operator' => '<',
                    'value' => 100000,
                ]
            ],
            '100rb-500rb' => [
                [
                    'column' => 'price',
                    'operator' => '>=',
                    'value' => 100000,
                ],
                [
                    'column' => 'price',
                    'operator' => '<',
                    'value' => 500000,
                ],
            ],
            '500rb-1jt' => [
                [
                    'column' => 'price',
                    'operator' => '>=',
                    'value' => 500000,
                ],
                [
                    'column' => 'price',
                    'operator' => '<',
                    'value' => 1000000,
                ],
            ],
            '1jt-5jt' => [
                [
                    'column' => 'price',
                    'operator' => '>=',
                    'value' => 1000000,
                ],
                [
                    'column' => 'price',
                    'operator' => '<',
                    'value' => 5000000,
                ],
            ],
        ]
    ],
];

