# Lapak: Scraper Marketplace di Indonesia

## License
Lapak mengikuti lisensi software dari Dojo
[Dojo License](https://public.3.basecamp.com/p/1WqtLBQGbGb6XW6ET23xGMqh).

Saran infrastruktur:
* Laravel Forge: Manajemen Server
* Digital Ocean: Web Hosting

Support saat ini:

* Tokopedia
* Bukalapak

## Berbagai worker yang perlu diawasi supervisord:

Tokopedia:
php artisan queue:listen redis --queue=tokopedia:sitemap_url_to_product_sitemaps --tries=3
php artisan queue:listen redis --queue=tokopedia:product_sitemap_to_product_urls --tries=3
php artisan queue:listen redis --queue=tokopedia:product_url_to_product --tries=3

Bukalapak:
php artisan queue:listen redis --queue=bukalapak:sitemap_url_to_categories --tries=3
php artisan queue:listen redis --queue=bukalapak:category_to_category_pages --tries=3 --timeout=300
php artisan queue:listen redis --queue=bukalapak:category_page_to_product_urls --tries=3
php artisan queue:listen redis --queue=bukalapak:product_url_to_product --tries=3


## Instruksi untuk memulai scraping:

* php artisan scrape:tokopedia
* php artisan scrape:bukalapak

Perintah di atas akan memerintahkan worker untuk memulai mengambil data. Worker diawasi supervisord agar bisa berjalan tanpa henti.
