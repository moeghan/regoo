<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

if(!class_exists('CreateSitemapsTable')){

    class CreateSitemapsTable extends Migration
    {
        /**
        * Run the migrations.
        *
        * @return void
        */
        public function up()
        {
            Schema::create('sitemaps', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();
                $table->string('loc');
                $table->string('lastmod');
                $table->integer('marketplace_id');
            });
        }

        /**
        * Reverse the migrations.
        *
        * @return void
        */
        public function down()
        {
            Schema::dropIfExists('sitemaps');
        }
    }
}
