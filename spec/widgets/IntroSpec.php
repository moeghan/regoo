<?php

describe('@widget("intro")', function(){
    $this->output = Widget::run('intro')->toHtml();
    it('should not produce empty output', function(){
        expect($this->output)->not->toBeEmpty();
    });
});
