<?php

describe('@widget("intro")', function(){
    $this->output = Widget::run('bannerCards')->toHtml();
    it('should not produce empty output', function(){
        expect($this->output)->not->toBeEmpty();
    });
});
