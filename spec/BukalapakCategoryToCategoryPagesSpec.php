<?php
use App\Marketplace;
use App\Jobs\BukalapakCategoryToCategoryPages;

describe('BukalapakCategoryToCategoryPages', function(){
    putenv('APP_ENV=testing');
    
    $this->scraper = new BukalapakCategoryToCategoryPages('https://www.bukalapak.com/c/komputer?from=category_home&search[keywords]=');

    describe('->handle()', function(){

        it('should enqueue bukalapak:category_page_to_product_urls', function(){
            $this->scraper->handle();
            $this->jobs = Redis::llen('queues:bukalapak:category_page_to_product_urls');
            expect($this->jobs)->not->toBeEmpty();
        });
    });

    after(function(){
        Artisan::call('migrate:refresh', ['--force' => true]);
        Redis::flushAll();
    });
});
