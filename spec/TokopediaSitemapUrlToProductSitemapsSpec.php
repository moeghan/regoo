<?php
use Carbon\Carbon;
use App\Marketplace;

describe('TokopediaSitemapUrlToProductSitemaps', function(){
    putenv('APP_ENV=testing');

    $sitemap_url = 'https://s3-ap-southeast-1.amazonaws.com/tokopedia-upload/sitemap/products-index.xml';

    $this->worker = new App\Jobs\TokopediaSitemapUrlToProductSitemaps($sitemap_url);
    describe('->handle()', function(){

        it('should enqueue product sitemaps', function(){
            $this->worker->handle();
            $this->jobs = Redis::llen('queues:tokopedia:product_sitemap_to_product_urls');
            expect($this->jobs)->not->toBeEmpty();
        });

    });

    after(function(){
        Artisan::call('migrate:refresh', ['--force' => true]);
        Redis::flushAll();
    });
});
