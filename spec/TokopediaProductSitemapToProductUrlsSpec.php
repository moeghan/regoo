<?php
use Carbon\Carbon;
use App\Marketplace;

describe('TokopediaProductSitemapToProductUrls', function(){
    putenv('APP_ENV=testing');
    
    $sitemap = [
        'loc' => 'https://s3-ap-southeast-1.amazonaws.com/tokopedia-upload/sitemap/product/990.xml.gz',
        'lastmod' => Carbon::now()
    ];

    $this->processor = new App\Jobs\TokopediaProductSitemapToProductUrls($sitemap);
    describe('->handle()', function(){

        it('should enqueue products', function(){
            $this->processor->handle();
            $this->jobs = Redis::llen('queues:tokopedia:product_url_to_product');
            expect($this->jobs)->not->toBeEmpty();
        });

    });

    after(function(){
        Artisan::call('migrate:refresh', ['--force' => true]);
        Redis::flushAll();
    });
});
