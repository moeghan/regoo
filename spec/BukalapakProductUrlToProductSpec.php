<?php
use Carbon\Carbon;
use App\Marketplace;
use App\Category;
use App\Product;
use App\Jobs\BukalapakProductsWorker;

describe('BukalapakProductUrlToProduct', function(){

    $this->product_url = 'https://www.bukalapak.com/p/komputer/mini-pc/1yw0vj-jual-remix-mini-pc?dtm_source=product_detail&dtm_section=detail-1&dtm_campaign=default';
    $this->processor = new App\Jobs\BukalapakProductUrlToProduct($this->product_url);

    describe('->handle()', function(){

        it('should create product with relevant categories', function(){
            $this->processor->handle();
            $product = Product::where('loc', '=', $this->product_url)->with('categories')->first();

            expect($product)->not->toBeNull();
            expect($product->categories()->count())->not->toBe(0);

        });

    });

    after(function(){
        Artisan::call('migrate:refresh', ['--force' => true]);
        Redis::flushAll();
    });
});
