<div class="panel panel-default">
    <div class="row">
        <div class="col-md-5">
            <a href="{{ $product->image }}"><img class="img-responsive" src="{{ $product->image }}" alt="{{ $product->name }}"></a>
        </div>
        <div class="col-md-7" style="padding: 0px 15px 0px 35px;">
            <h3 style="margin-top: 10px;">{{ $product->name }}</h3>
            <p>
                <i class="glyphicon glyphicon-tags"></i>&nbsp;
                @foreach($product->categories as $category)
                        
                    {{ $category->name }} 
                        
                    @if($product->categories->last() !== $category)
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    @endif
                @endforeach


            </p>
            <p>
	            <i class="glyphicon glyphicon-map-marker"></i> {{ $product->location }} &nbsp;
				<i class="glyphicon glyphicon-time"></i> {{ $product->created_at->diffForHumans() }}
            </p>
            <p class="money" style="margin-top:10px; font-size: 2em;">{{ format_money($product->price) }}</p>
			<table class="table table-detail">
		    	<thead>
		    		<tr>
		    			<th>Toko</th>
		    			<th>Situs</th>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<tr>
		    			<td width="25%">
		    				<a href="{{ route('marketplace.show', ['slug' => $product->marketplace->slug]) }}"><img class="img-responsive" src="{{ $product->marketplace->logo }}" alt="{{ $product->marketplace->name }}"></a>
		    			</td>
		 
		        		<td>
		        			<div class="btn-group btn-group-justified" role="group" aria-label="buka situs"> 
		        				@widget('likeButton', ['product' => $product])
		        				<a href="{{ route('product.redirect', ['u' => $product->url]) }}" rel="nofollow" target="_blank" class="btn btn-default btn-danger" title="{{ $product->name }}"><i class="fa fa-chevron-right"></i>  Buka Situs</a>
		        		</td>
		        	</tr>
		        </tbody>
		    </table>

        </div>
	</div>
</div>

<div class="panel panel-info" id="deskripsi">
	<div class="panel-heading"> 
        Deskripsi Barang
    </div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
			    
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style="padding:10px 0px 10px 25px;max-height:150px;overflow-y:scroll;width:100%;">
				
		    	{!! strip_tags($product->description, '<p><br>') !!}
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-md-12">
        <h2>Produk Sejenis <small>{{ $product->name }}</small></h2>
    </div>
</div>
<div class="infinite-scroll">
	@widget('products', [
		'products' => $related_products,
		'view_as' => 'list',
		'title' => ''
	])
</div>
<div class="text-center" id="pagination">
	{{ $related_products->links() }}
</div>

<div class="row" style="margin-bottom:20px;">
	<div class="col-md-6 col-md-offset-3">
			<a class="btn btn-success btn-block" href="{{ route('product.index', ['query' => strtolower($product->name)]) }}"><i class="fa fa-search"></i> Bandingkan Semua Produk Sejenis</a>

	</div>
</div>