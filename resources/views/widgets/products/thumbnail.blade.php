<div class="widget products">
    <div class="row">
        <div class="col-md-12">
            @if($config['title'])
            <h1>
                {{ $config['title'] }}
            </h1>
            @endif
        </div>
    </div>
    @foreach($config['products']->chunk($config['chunk']) as $chunked_products)
        <div class="{{ $config['chunk_class'] }}">
            @foreach($chunked_products as $key => $product)
                <div class="{{ $config['item_class'] }}">
                    <div class="thumbnail">
                        <a href="{{ build_product_url_for($product) }}"><img src="{{ $product->image->resize(200,200) }}" alt="{{ $product->name }}"></a>
                        <div class="caption" style="font-size:12px;">
                            <p><a href="{{ build_product_url_for($product) }}">{{ $product->name }}</a></p>
                            <p style="color:#a30046; font-size: 14px;">
                                <strong>{{ format_money($product->price_reduced) }}</strong>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach
</div>
