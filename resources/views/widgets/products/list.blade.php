<div class="widget products list">
    <div class="row">
        <div class="col-md-12">
            @if($config['title'])
            <h1>
                {{ $config['title'] }}
            </h1>
            @endif
        </div>
    </div>
    @foreach($config['products'] as $product )
    <div class="panel panel-default">
        <div class="row">
            <div class="col-md-3">
                <a href="{{ build_product_url_for($product) }}"><img class="img-responsive" src="{{ $product->image->resize(175,175) }}" alt="{{ $product->name }}"></a>
            </div>
            <div class="col-md-6">
                <h3 style="margin-top: 10px;"><a href="{{ build_product_url_for($product) }}">{!! highlight(ucwords($product->name), $q) !!}</a></h3>
                <p>{!! highlight(str_limit(strip_tags($product->description), 170), $q) !!}</p>
                <p>
                    <i class="glyphicon glyphicon-tags"></i>&nbsp;
                    @foreach($product->categories as $category)
                        <a href="{{ route('product.index', ['categories:slug' => str_slug($category->name)]) }}" class="">
                            
                            {{ $category->name }} 
                            
                        </a>
                        @if($product->categories->last() !== $category)
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        @endif
                    @endforeach
                </p>
                <p><a href="{{ route('product.index', ['location' => strtolower($product->location)]) }}"><i class="glyphicon glyphicon-map-marker"></i> {{ $product->location }}</a>
                
                </p>
            </div>
            <div class="col-md-2">
                <p class="money" style="margin-top:10px;">{{ format_money($product->price) }}</p>
                <p>
                    <a href="{{ route('marketplace.show', ['slug' => $product->marketplace->slug]) }}"><img class="img-responsive" src="{{ $product->marketplace->logo }}" alt="{{ $product->marketplace->name }}"></a>

                    <a href="{{ build_product_url_for($product) }}" rel="nofollow" class="btn btn-product" title="{{ $product->name }}">Lihat Produk</a> 
                </p>

                <p>
                    <i class="glyphicon glyphicon-time"></i> {{ $product->created_at->diffForHumans() }}
                </p>
                <p>
                    @widget('likeButton', ['product' => $product])
                </p>

            </div>
        </div>
        
    </div>
    @endforeach
</div>
