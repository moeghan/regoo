<div class="row" style="margin-top:20px;">
    <div class="col-md-5">
        <a href="{{ route('product.index', ['categories:slug' => 'handphone', 'sort' => 'price,desc']) }}" class="thumbnail"><img class="img-responsive" src="{{ url('images/banners/rsz_sales_hp_idaman_375x350-01.jpg') }}" alt=""></a>
    </div>
    <div class="col-md-5">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ route('product.index', ['query' => 'korea', 'categories:slug' => '%fashion%']) }}" class="thumbnail"><img class="img-responsive" src="{{ url('images/banners/rsz_sales_tampil_gaya_ala_korea_d45__375x170-07.jpg') }}" alt=""></a>
            </div>
            <div class="col-md-6">
                <a href="{{ route('product.index', ['query' => 'kesehatan pria']) }}" class="thumbnail"><img class="img-responsive" src="{{ url('images/banners/rsz_sales_finalharga_mulai_180x170-11.jpg') }}" alt=""></a>
            </div>
            <div class="col-md-6">
                <a href="{{ route('product.index', ['categories:slug' => '%mainan%']) }}" class="thumbnail"><img class="img-responsive" src="{{ url('images/banners/%28SALES%29-%28BANNER-PRIA%29-Aneka-Mainan-Harga-Mulai-45-Ribuan-180x170.jpg') }}" alt=""></a>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <a href="{{ route('product.index', ['categories:slug' => '%hadiah%']) }}" class="thumbnail"><img class="img-responsive" src="{{ url('images/banners/SALES_BAJU_ANAK_180x350_%281%29.jpg') }}" alt=""></a>
    </div>
</div>
