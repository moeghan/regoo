@if(Auth::guest())
    <a rel="nofollow" href="#login-overlay" class="btn btn-default btn-small" data-toggle="modal" data-target="#login-overlay"><i class="fa fa-heart-o" aria-hidden="true"></i>&nbsp;{{ config('site.like.label') }}</a>

@else

    @if($product->liked())
        <a rel="nofollow" href="{{ route('product.unlike', ['id' => $product->id ]) }}" data-disable data-method="post" data-remote="true" class="btn btn-success btn-small btn-like"><i class="fa fa-bookmark" aria-hidden="true"></i> {{ config('site.like.label') }}
            <span class="like-count badge">
            @if($product->likeCounter)
            {{ $product->likeCounter->count }}
            @else
            0
            @endif
            </span>
        </a>
    @else
        <a rel="nofollow" href="{{ route('product.like', ['id' => $product->id ]) }}" data-disable data-method="post" data-remote="true" class="btn btn-default btn-small btn-like"><i class="fa fa-bookmark" aria-hidden="true"></i> {{ config('site.like.label') }}
            <span class="like-count badge">
                @if($product->likeCounter)
                {{ $product->likeCounter->count }}
                @else
                0
                @endif
            </span>
        </a>
    @endif

@endif