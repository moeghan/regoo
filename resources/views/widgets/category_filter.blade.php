<h4>Kategori</h4>
@if(isset($config['category']->parent))
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ build_category_parent_url() }}"><i class="fa fa-chevron-left"></i> {{ $config['category']->parent->name }}</a></li>
    </ul>
@else
	@if(isset($config['category']))
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('marketplace.show', ['slug' => $config['marketplace']->slug]) }}"><i class="fa fa-chevron-left"></i> {{ $config['marketplace']->name }}</a></li>
    </ul>   
    @endif
@endif
@if(!is_null($config['category']))
<ul class="list-group">
	<li class="list-group-item active"><i class="fa fa-chevron-down"></i> {{ $config['category']->name }}</li>

    @foreach($config['category']->children()->remember(10080)->get() as $category)
    <li class="list-group-item">
        <a href="{{ build_category_url($category->slug) }}">{{ $category->name }}</a>
    </li>
    @endforeach
</ul>
@else
	<ul class="list-group">
    @foreach($categories as $category)
        <li class="list-group-item">
            <a href="{{ route('category.show', ['marketplace' => $config['marketplace']->slug, 'hierarchy' => $category->slug]) }}">{{ $category->name }}</a>
        </li>
    @endforeach
</ul>
@endif