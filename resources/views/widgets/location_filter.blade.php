<div class="list-group">
	<div href="#" class="list-group-item active">
    	<span class="panel-title">
			<i class="glyphicon glyphicon-map-marker"></i> Pilih Lokasi
		</span>
  	</div>
	@foreach($locations as $location => $count)
    <a href="{{ build_filter(['location' => strtolower($location)]) }}" class="list-group-item"><span class="badge">
    	@if($ratio <= 1)
    	{{ rp_terbilang($count) }}
    	@else
    	{{ rp_terbilang($count*$ratio) }}
    	@endif
    </span>
    {{ $location }}</a>
    @endforeach
</div>