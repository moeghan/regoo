<div class="row">
    <div class="col-md-12">
        <div class="text-center alert alert-info">
            <h1>Bandingkan Sebelum Belanja</h1>
            <p style="font-size: 1.3em;">
                {{ config('site.name') }} Mengindex <strong style="color: #b30143;">{{ $product_count }}</strong> produk dari {{ App\Marketplace::remember(5)->count('id') }}  marketplace di Indonesia

            </p>
            <div class="share-icons"></div>
        </div>
    </div>
</div>
