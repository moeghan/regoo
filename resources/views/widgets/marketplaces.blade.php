<div class="row" style="margin-top:20px;">

    @foreach(App\Marketplace::remember(5)->get() as $marketplace)
    <div class="col-md-2">
        <a href="{{ route('marketplace.show', [$marketplace->slug]) }}">
            <img class="img-responsive" src="{{ $marketplace->logo }}" alt="{{ $marketplace->name }}" />
        </a>
    </div>
    @endforeach

    @foreach(getLogos() as $logo)
    <div class="col-md-2">
        <img class="img-responsive desaturate" src="{{ url($logo) }}">
    </div>
    @endforeach
</div>
