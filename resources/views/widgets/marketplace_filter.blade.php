<div class="list-group">
	<div href="#" class="list-group-item active">
    	<span class="panel-title">
			<i class="glyphicon glyphicon-shopping-cart"></i> Pilih Marketplace
		</span>
  	</div>
	@foreach($marketplaces as $marketplace)
    <a href="{{ build_filter(['marketplace:slug' => $marketplace->slug]) }}" class="list-group-item">
    <span class="badge">
    	@if($ratio <= 1)
    	{{ rp_terbilang($marketplace->total) }}
    	@else
    	{{ rp_terbilang($marketplace->total*$ratio) }}
    	@endif
    </span>
    {{ $marketplace->name }}</a>
    @endforeach
</div>