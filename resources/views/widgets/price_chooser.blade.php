<div class="list-group">
	<div href="#" class="list-group-item active">
    	<span class="panel-title">
			<i class="fa fa-dollar"></i> Pilih Harga
		</span>
  	</div>
	@foreach($prices as $price)
    <a href="{{ build_filter(['price[min]' => $price->min, 'price[max]' => $price->max]) }}" class="list-group-item">
    <span class="badge">
    	@if($ratio <= 1)
    	{{ rp_terbilang($price->total) }}
    	@else
    	{{ rp_terbilang($price->total*$ratio) }}
    	@endif
    </span>
    {{ $price->round_price }}</a>
    @endforeach
</div>