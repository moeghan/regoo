@extends('layouts.app')

@section('content')
    <div class="row">
        
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-primary"> 
                                <div class="panel-heading"> 
                                    {!! Breadcrumbs::render('user_product_favorite')!!}
                                    <!-- breadcrumb -->
                                    <div class="pull-right">
                                        View as: &nbsp;
                                        <div class="btn-group">
                                            <a rel="nofollow" href="{{ build_filter(['view_as' => 'list']) }}" class="white"><i class="glyphicon glyphicon-th-list" aria-hidden="true"></i> List</a>

                                            &nbsp;

                                            <a href="{{ build_filter(['view_as' => 'thumbnail']) }}" class="white"><i class="glyphicon glyphicon-th" aria-hidden="true"></i> Thumbnail</a>
                                        </div>

                                    </div>
                                </div> 
                                
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h1>{{ $title }}</h1>
                            <hr>
                        </div>
                    </div>
                    <div class="infinite-scroll">
                        @if($products->count() > 0)
                        @widget('products', [
                            'title' => '',
                            'products' => $products,
                            'item_class' => 'col-md-3',
                            'chunk_class' => 'row',
                            'chunk' => 4,
                            'view_as' => request()->input('view_as', 'list')

                        ])
                        @else
                        <p>Belum ada produk favorit. Silakan <a href="{{ url('products') }}">jelajahi dulu produknya</a>, lalu klik tombol favorit.</p>
                        
                        <div class="col-md-6 col-md-offset-3">
                            
                            <a href="{{ url('products') }}" class="btn btn-block btn-primary">
                                Lihat produk di {{ config('site.name') }}
                            </a>
                        </div>
                        
                        @endif
                    </div>
                    
                    <div class="text-center" id="pagination">
                        {{ $products->links() }}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    {{ $user->name }}
                </div>
                <div class="panel-body">
                    <center>
                        <img class="img-responsive img-thumbnail" src="{{ $user->avatar }}" alt="{{ $user->name }} mascot"><br>
                        
                    </center>
                    <br>
                    <a href="{{ url('edit-profile') }}" class="btn btn-info btn-block">Ubah Profil</a>
                </div>
            </div>
    
            <!-- todo:  productSorter widget -->            

        </div>
    
    </div>
@endsection

@section('script')
$(document).ready(function(){
    $('.dropdown-submenu a.test').on("click", function(e){
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });
    
    $('.infinite-scroll').infinitescroll({
 
        navSelector  : "div#pagination",            
                       // selector for the paged navigation (it will be hidden)
        nextSelector : 'ul.pager li a[rel="next"]',    
                       // selector for the NEXT link (to page 2)
        itemSelector : "div.products"          
                       // selector for all items you'll retrieve
    });

});
@endsection