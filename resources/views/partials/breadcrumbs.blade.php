@if ($breadcrumbs)
    @foreach ($breadcrumbs as $breadcrumb)
        @if (!$breadcrumb->last)
            <a class="white" href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a> &nbsp; / &nbsp; 
        @else
            <span style="color: #eee;">{{ $breadcrumb->title }}</span>
        @endif
    @endforeach
@endif