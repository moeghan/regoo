<form class="navbar-form" action="{{ route('product.index') }}">
    <div class="form-group" style="display:inline;">
        <div class="input-group" style="display:table;">


            <div class="input-group-btn" style="width:1%;">
                <select name="marketplace:slug" class="form-control">
                    <option value="%%">Semua Marketplace</option>

                    @foreach(App\Marketplace::all() as $marketplace)
                    <option 

                    @if(request()->input('marketplace:slug', '') == $marketplace->slug)
                    selected="selected"
                    @endif
                      
                      value="{{ $marketplace->slug }}">{{ $marketplace->name }}</option>
                    @endforeach

                </select>
            </div>
            <input type="text" name="query" class="form-control" value="@if(request()->input('query')){{ request()->input('query', '') }}@endif">
            <div class="input-group-btn" style="width:1%;">
                <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i> Cari</button>
            </div>
      </div>

      <div class="row text-center">
          <div class="col-md-10 col-md-offset-1">
              <?php $last_term = get_term();?>
              @if(!is_null($last_term))
              Contoh Pencarian: <a href="{{ url($last_term['url']) }}">{{ $last_term['term'] }}</a>
              @endif
          </div>
      </div>
    </div>
 </form>