<div  id="login-overlay" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="login-overlay">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Masuk ke {{ config('site.name') }}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="well" style="padding: 30px;">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="control-label">E-Mail Address</label>

                                    <div class="">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="control-label">Password</label>

                                    <div class="">
                                        <input id="password" type="password" class="form-control" name="password">

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember"> Remember Me
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success btn-block">
                                            <i class="fa fa-btn fa-sign-in"></i> Login
                                        </button>

                                        <a class="btn btn-default btn-block" href="{{ url('/password/reset') }}">Lupa Password?</a>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <p class="lead">Belum punya akun? </p>
                        <ul class="list-unstyled" style="line-height: 2">
                            <li><span class="fa fa-check text-success"></span> Daftar sekarang <span class="text-success">GRATIS</span></li>
                            <li><span class="fa fa-check text-success"></span> Simpan semua produk favorit</li>
                            <li><span class="fa fa-check text-success"></span> Simpan pencarian favorit (coming soon)</li>
                            <li><span class="fa fa-check text-success"></span> Notifikasi perubahan harga (coming soon)</li>
                        </ul>
                        <p><a href="{{ url('register') }}" class="btn btn-info btn-block">Daftar Sekarang</a>
                        </p>
                        <h5>Atau Masuk/Daftar Memakai Medsos</h5>
                        <a href="{{ route('social.redirect', ['provider' => 'facebook']) }}" class="btn btn-small btn-primary btn-block facebook" type="submit">

                        <i class="fa fa-facebook-f"></i>
                        
                        Masuk dengan Facebook</a>

                        <a href="{{ route('social.redirect', ['provider' => 'google']) }}" class="btn btn-small btn-default btn-block google" type="submit">
                        <i class="fa fa-google"></i>

                        Masuk dengan Google</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>