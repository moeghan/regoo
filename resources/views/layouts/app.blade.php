<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {!! SEO::generate() !!}

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="csrf-param" content="_token" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.2.1/jssocials.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.2.1/jssocials-theme-flat.css" />


    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }

        @media (min-width: 1200px) {
            .container{
                max-width: 970px;
            }
        }
        html {
          position: relative;
          min-height: 100%;
        }
        body {
          /* Margin bottom by footer height */
          margin-bottom: 60px;
          background-color: #fefefe;
        }
        .footer {
          position: absolute;
          bottom: 0;
          width: 100%;
          /* Set the fixed height of the footer here */
          height: 60px;
          background-color: #f5f5f5;
        }

        .container .text-muted {
          margin: 20px 0;
        }

        img.desaturate {
            filter: gray; /* IE6-9 */
            filter: grayscale(1); /* Microsoft Edge and Firefox 35+ */
            -webkit-filter: grayscale(1); /* Google Chrome, Safari 6+ & Opera 15+ */
        }

        .widget.products > .row > .col-md-2{
            padding-left: 5px;
            padding-right: 5px;
        }
        .widget.products > .row > .col-md-2:first-child{
            padding-left: 15px;
        }

        .widget.products > .row > .col-md-2:last-child{
            padding-right: 15px;
        }

        h1,h2,h3{
            color:#b30143;
            font-size: 2em;
            margin-top: 0px;
        }

        h2{
            font-size: 1,5em;            
        }
        h3{
            font-size: 1.5em;            
        }

        .money{
            color:#a30046;
            font-weight: bold;
        }

        .btn-product {
            background: #ff5722;
            color: #ffffff;
            padding: 4px 8px;
            font-size: 12px;
            text-transform: uppercase;
            font-family: 'Roboto',sans-serif;
            font-weight: 400;
            position: relative;
            margin-right: 17px;
            margin-top: 10px;
        }

        .btn-product:after {
            content: "";
            display: inline-block;
            width: 0;
            height: 0;
            margin-left: 2px;
            vertical-align: middle;
            border-top: 12px solid transparent;
            border-bottom: 13px solid transparent;
            border-right: 16px solid transparent;
            border-left: 12px solid #ff5722;
            position: absolute;
            top: 0;
            right: -28px;
        }

        .btn-product:hover{
            color: #eee;
        }

        .white, .white a {
            color: #fff;
            text-decoration: none;
        }
        .white:hover, .white a:hover {
            color: #eee;
            text-decoration: none;
        }

        .panel-heading h1{
            font-weight: 1em;
        }

        .dropdown-sidebar-menu {
            top: 0;
            left: 100%;
        }

        h1#product-title{
            font-size: 1.5em;
        }
        .search-highlight{
        }

    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container" style="margin-top:15px;">

            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('site.name') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right" style="margin-left: 10px;">
                    @if (Auth::guest())
                        <li>
                        <p class="navbar-btn">
                            <div class="btn-group">
                            <a class="btn btn-default btn-small" href="#login-overlay"  data-toggle="modal" data-target="#login-overlay">Login</a>
                            <a class="btn btn-default btn-primary btn-small" href="{{ url('/register') }}">Register</a>
                                
                            </div>
                            </p>
                        </li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <img style="width: 40px;margin-top: -10px;border-radius: 40px;" src="{{ Auth::user()->avatar }}" alt=""> 
                                
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('dashboard') }}"><i class="fa fa-btn fa-bookmark"></i> Produk Favorit</a></li>
                                <li><a href="#"><i class="fa fa-btn fa-search"></i> Pencarian Favorit (soon)</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i> Logout</a></li>
                            </ul>
                        </li>
                    @endif

                </ul>

                @include('partials.searchform')
            </div>
        </div>
    </nav>

    <div class="container">
        @yield('content')
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted">
                    Perlu diketahui bahwa kami tidak dapat menjamin semua informasi yang ditampilkan seperti harga, spesifikasi, gambar, dll adalah 100% akurat.
                </p>
            </div>
        </div>
        @include('auth.loginmodal')
    </div>
    <footer class="footer">
      <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted">
                    Copyright &copy; {{ date('Y') }} {{ config('site.name') }}
                    <span class=" pull-right">
                        Contact Us | Report
                    </span>
                </p>
            </div>
        </div>
      </div>
    </footer>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.2.1/jssocials.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-infinitescroll/2.1.0/jquery.infinitescroll.min.js"></script> 
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ujs/1.2.1/rails.min.js"></script>
   

    <script type="text/javascript">
        @yield('script')

        $('.btn-like').bind('ajax:success', function(e, data, status, xhr){
            var $btn = $(this);
            $btn.find('span.like-count').text(data);
            $btn.toggleClass('btn-default btn-success');

            $btn.find('i').removeClass('fa-bookmark-o');
            $btn.find('i').addClass('fa-bookmark');
        });

        $('.btn-unlike').bind('ajax:success', function(e, data, status, xhr){
            var $btn = $(this);

            $btn.find('span.like-count').text(data);

            $btn.addClass("btn-default btn-success");

            $btn.find('i').removeClass('fa-bookmark');
            $btn.find('i').addClass('fa-bookmark-o');
        });

    </script>

    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
