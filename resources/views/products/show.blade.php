@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<h1>{{ $title }}</h1>
		</div>
	</div>
	<div class="row">
		
	

		<div class="col-md-9">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-primary"> 
						        <div class="panel-heading"> 
									{!! Breadcrumbs::render('product', $product) !!}
						        </div> 
						        
						    </div>
						</div>
					</div>
					@widget('product', ['product' => $product, 'related_products' => $related_products])
				</div>
			</div>
		</div>

		<div class="col-md-3 hidden-xs hidden-sm">
			<div class="panel panel-default text-center">
				<a href="{{ route('marketplace.show', ['slug' => $marketplace->slug]) }}"><img class="img-responsive" src="{{ $marketplace->mascot }}" alt="{{ $marketplace->name }} mascot"></a>
				{{ $marketplace->slug }}
			</div>

			@widget('categoryFilter', ['marketplace' => $marketplace])
			@widget('productFilter')


		</div>
	</div>
@endsection

@section('script')
$(document).ready(function(){
	$('.dropdown-submenu a.test').on("click", function(e){
		$(this).next('ul').toggle();
    	e.stopPropagation();
	    e.preventDefault();
	});

	$('.infinite-scroll').infinitescroll({
 
	    navSelector  : "div#pagination",            
	                   // selector for the paged navigation (it will be hidden)
	    nextSelector : 'ul.pager li a[rel="next"]',    
	                   // selector for the NEXT link (to page 2)
	    itemSelector : "div.products"          
	                   // selector for all items you'll retrieve
	});
});
@endsection