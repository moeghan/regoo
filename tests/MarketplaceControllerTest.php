<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MarketplaceControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testShow()
    {
        $this->visit('sites/tokopedia.com')
        ->assertResponseOk()
        ->see('Tokopedia');

        $this->visit('sites/tokopedia.com?view_as=list')
        ->seeElement('div.list');
    }
}
