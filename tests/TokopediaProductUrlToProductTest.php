<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Carbon\Carbon;

class TokopediaProductUrlToProductTest extends TestCase
{
	private $product;
	private $processor;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHandle()
    {
        $this->product = [
	        'loc' => 'https://www.tokopedia.com/ibnuminatimerch/boardshort-ripcurl-original-cps-ripcurl-384',
	        'lastmod' => new Carbon('2016-07-18T08:46:50+08:00')
	    ];

	    $this->processor = new App\Jobs\TokopediaProductUrlToProduct($this->product);
        $this->processor->handle();

    }
}
