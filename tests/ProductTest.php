<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Product;

class ProductTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUrlBuilder()
    {
        $url = build_product_url_for(1);

        $this->assertNotEmpty($url);
    }

    public function testFulltextSearch()
    {
        $products = Product::whereRaw('MATCH (name,description) AGAINST(?)', ['macbook air 11']);

        $this->assertTrue($products->count() > 0);

    }
}
