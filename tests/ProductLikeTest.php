<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Product;

class ProductLikeTest extends TestCase
{
    public function testLikeButtonShouldAppear()
    {
    	$like_label = config('site.like.label');

    	$this->visit('products')
    	->see($like_label);
    }

    public function testLikeButtonWidget()
    {
    	$output = Widget::run('likeButton', ['product' => Product::find(1)])->toHtml();

    	$this->assertNotEmpty($output);
    }
}
