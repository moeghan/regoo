<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testShow()
    {
        $product = App\Product::find(1);
        $this->visit(build_product_url_for($product->id))
        ->assertResponseOk()
        ->see($product->name);
    }

    public function testFilterByLocation()
    {
        $this->visit('products?location=bandung')
        ->assertResponseOk()
        ->see('bandung');
    }
}
