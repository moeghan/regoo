<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Category;

class CategoryControllerTest extends TestCase
{
	public function testShow()
	{
		
	    $this->visit('sites/tokopedia.com/handphone-tablet')
	    ->assertResponseOk()
	    ->see('Handphone & Tablet');
	    $this->visit('sites/tokopedia.com/handphone-tablet/aksesoris-handphone')
	    ->assertResponseOk();
	    $this->visit('sites/tokopedia.com/handphone-tablet/aksesoris-handphone/casing-cover')
	    ->assertResponseOk();

	}
}
